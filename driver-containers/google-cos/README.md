# Introduction

This document captures some notes when building drivers for use with 
Google-COS for the `cos-beta` milestone.

## Steps

Provision a Google COS VM instance on GCP with T4 attached to the VM. Use `cos-beta-93-16623-39-1` as the current COS image.

List all the available COS images:
```bash
$ gcloud compute images list | grep cos-cloud
cos-81-12871-1317-1                                   cos-cloud            cos-81-lts                                    READY
cos-85-13310-1308-22                                  cos-cloud            cos-85-lts                                    READY
cos-89-16108-534-8                                    cos-cloud            cos-89-lts                                    READY
cos-beta-93-16623-39-1                                cos-cloud            cos-beta                                      READY
cos-dev-97-16695-0-0                                  cos-cloud            cos-dev                                       READY
cos-stable-89-16108-534-8                             cos-cloud            cos-stable                                    READY
```

Build the cos-docker build image:

```bash
docker build -t cos-docker-93-16623-39-1 .
```

Download the driver and place it under `/var/lib/nvidia` which is bind-mounted into the container. 

```bash
$ BASE_URL=https://us.download.nvidia.com/tesla
$ DRIVER_VERSION=450.102.04
$ curl -fSsl -O $BASE_URL/$DRIVER_VERSION/NVIDIA-Linux-x86_64-$DRIVER_VERSION.run
$ mv NVIDIA-Linux-x86_64-450.102.04.run /var/lib/nvidia/
$ sudo mount --bind /var/lib/nvidia /var/lib/nvidia
$ sudo mount -o remount,exec,rw /var/lib/nvidia
```

Now run the `cos-docker` image built on the previous step to launch the build container: 

```bash
$ docker run \
     --privileged \
     --net=host \
     --pid=host \
     --volume /var/lib/nvidia:/usr/local/nvidia \
     --volume /dev:/dev \
     --volume /:/root \ 
    cos-docker-93-16623-39-1
```

The error can be seen below after a few minutes:

```bash
  CC       /build/usr/src/linux/tools/objtool/librbtree.o
  LD       /build/usr/src/linux/tools/objtool/objtool-in.o
  LINK     /build/usr/src/linux/tools/objtool/objtool
  DESCEND  bpf/resolve_btfids
  MKDIR     /build/usr/src/linux/tools/bpf/resolve_btfids//libbpf

Auto-detecting system features:
...                        libelf: [ on  ]
...                          zlib: [ OFF ]
...                           bpf: [ on  ]

No zlib found
Makefile:290: recipe for target 'zdep' failed
make[3]: *** [zdep] Error 1
make[2]: *** [/build/usr/src/linux/tools/bpf/resolve_btfids//libbpf/libbpf.a] Error 2
Makefile:44: recipe for target '/build/usr/src/linux/tools/bpf/resolve_btfids//libbpf/libbpf.a' failed
Makefile:71: recipe for target 'bpf/resolve_btfids' failed
make[1]: *** [bpf/resolve_btfids] Error 2
make: *** [tools/bpf/resolve_btfids] Error 2
Makefile:1947: recipe for target 'tools/bpf/resolve_btfids' failed

```