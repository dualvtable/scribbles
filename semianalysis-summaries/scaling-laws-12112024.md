# Scaling Laws 

Semianalysis.com article summary from Dec 11, 2024: [link](https://semianalysis.com/2024/12/11/scaling-laws-o1-pro-architecture-reasoning-training-infrastructure-orion-and-claude-3-5-opus-failures/)

## Key Points

1. This article is a defense piece against some in the industry claiming that AI has hit the scaling wall 
1. Uses Moore's law to vindicate that when one dimension (clock speeds) stopped scaling, scaling was done using other dimensions, for example multi-cores, I/O scaling, higher bandwidth networking, parallel processing architectures etc. 
1. Similarly, there are more dimensions for scaling beyond focusing on pre-training -- other areas include data generation, promixal policy optimization (PPO), functional verifiers, reasoning models, improving inference serving economics etc. 
1. If insufficient data is available for training models, models become over-parameterized and inefficient (i.e. memorization rather than generalization) => requiring use of synthetic data to alleviate this problem
1. Video seems to be the new frontier -- contianing quadrillions of alternative tokens but this requires scaling of training FLOPs (and potentially require multi-datacenter training infrastructure) as publicly available text resources are exhausted on the internet
1. Models that become too dense have unfavorable inference serving economics
1. Argues that using latest models for synthetic data generation allows newer foundational models to be either fine-tuned or trained faster (Anthropic using Claude 3.5 Opus to generate synthetic data to improve Claude 3.5 Sonnet)
1. Synthetic data generation techniques for use in supervised fine-tuning (SFT) during the post-training step
    <ol type="a">
    <li> Rejection sampling </li>
    <li> Judgement by model </li>
    <li> Long context datasets (where models are pre-trained using capped context lengths as longer sequence lengths means larger KVCache to keep in memory; models can be used to summarize large pieces of text chunked into capped context length)</li>
    <li> Reinforcement Learning -- with human feedback (HF) vs. AI feedback (AF). See this [paper](https://arxiv.org/pdf/2309.00267) from Google </li>
    </ol>
1. RL is a key part of developing reasoning models that use Chain of Thought (CoT). Reasoning Models break the response into a discrete number of reasoning step called a Chain-of-Thought, before delivering a response to the user. Reasoning models can backtrack if they reach an illogical conclusion, recognizing that a mistake has been made or a certain approach has reached a dead end, revisiting earlier steps to put the chain of reasoning back on the right path.
   <ol type="a">
   <li>See this [paper](https://arxiv.org/pdf/2305.20050) from OpenAI on the two types of reward models -- outcome reward model (ORM) which provide an reward based on the outcome and process reward model (PRM) which provide a reward based on the process: </li>
      > ORMs typically work by ranking a variety of different answers that a model provides and then selecting the highest ranked one. In contrast, PRMs evaluate and assign a score to each step of the reasoning chain of thought and provide a reward based on this score and for this reason are generally preferred when training Chain of Thought models.
    </ol>
1. Why should we care about reasoning models? In other words, what are the implications on AI infra esp. large interconnected GPU systems?
   <ol type="a">
   <li> A significant improvement in model performance on challenging tasks oriented around coding, math and science</li>
   <li> Improvement in model performance scales with test-time compute (i.e. compute at inference time) extends robustly to LLMs </li>
   <li> During inference, reasoning capabilities are bottlenecked by long context lengths which increase memory & compute requirements. 
      <ol type="i">
      <li> Increased sequence length results in much greater memory (KVCache size is linearly proportional to increasing batch size * sequence length => having high number of users generating long sequence lengths leads to large KVCache size, bandwidth and also reduces interactivity) and FLOP requirements per token (FLOP scales quadratically with respect to sequence length). Economics wise, this means that to maintain interactivity, the system needs to serve smaller number of users (smaller batch sizes) to amortize the TCO for serving </li>
      </ol>
   <li> Thus serving systems are forced to cap the context length to keep serving costs economical at reasonable latency. High perf systems such as GB200 NVL72 could significantly change this inference serving equation -- in other words, allowing longer reasoning chains without being constrained by compute or memory requirements (i.e. the test-time compute scaling dimension) </li>
   <li> Reasoning models face reliability issues -- e.g. if SDC or other faults occur at inference, each additional token generated is appended to prior tokens before being passed through the model again. This corrupted token becomes part of the context window of the conversation potentially causing model errors. </li>
   </ol>



## References

### Definitions

1. Pre-training (see LLama3.1 [paper](https://arxiv.org/pdf/2407.21783))
   > Language model pre-training involves: (1) the curation and filtering of a large-scale training corpus, (2) the
development of a model architecture and corresponding scaling laws for determining model size, (3) the
development of techniques for efficient pre-training at large scale, and (4) the development of a pre-training
recipe.
1. Chinchilla scaling
   > Chinchilla scaling refers to the optimal increases in data versus parameter counts relative to increases in compute. Not enough data causes the model to generalize poorly, while too much data results in overtraining, which wastes compute resources. There are some instances where deviating from the optimal ratio makes sense: over-training models (e.g. GPT-4o and Llama) can decrease inference costs significantly and is preferrable for providers that have a larger user base to serve said model to.