# Introduction

Quick notes from reading the technical [paper](https://ai.meta.com/research/publications/the-llama-3-herd-of-models/) on Llama3.1
Date: 07/26/2024

## Model Architecture
1. Uses a standard dense Transformer model architecture with minor adaptations, 
   rather than for a mixture-of-experts model to "maximize training stability". 
   The adaptations include:
   <ol type="a">
   <li>use GQA with 8 key-value heads to improve inference speed and to reduce
   the size of key-value caches during decoding.</li>
   <li>use an attention mask that prevents self-attention between different 
   documents within the same sequence for pre-training on very long sequences</li>
   <li>vocabulary of 128K tokens</li>
   <li>increase RoPE base frequency hyperparameter to 500K for longer contexts </li>

1. Claims that Llama 3 outperforms MoE models, suggesting that dense architectures are not the limiting factor, but there remain numerous trade offs in terms of training and inference efficiency, and model stability at scale.

## Training Infra
1. Llama3.1 was trained on Meta's [production clusters](https://engineering.fb.com/2024/03/12/data-center-engineering/building-metas-genai-infrastructure/) (rather than FAIR) for reliability reasons. 
1. Compute
   <ol type="a">
   <li>405B was trained on "up to" 16K H100 GPUs at 700W TDP with 80GB HBM3 -- server based on Grand Teton design with HGX baseboards</li>
   <li>💡 NOTE: this paragraph appears to conflict with the next one in the paper that states the 405B cluster had 24K GPUs, but a footnote reads that pre-training used only 16K/24K GPUs </li>
   </ol>
1. Storage
   <ol type="a">
   <li>240PB of storage served by 7.5K servers with SSDs -- for 2TB/s sustained (peak 7TB/s) throughput</li>
   <li>See this talk on the storage fabric: https://atscaleconference.com/videos/training-llama-a-storage-perspective/</li>
   <li>Supports bursty traffic due to checkpoints -- checkpoints of the model state range from 1MB - 4GB per GPU</li>
   </ol>
1. Network
   <ol type="a">
   <li>405B was trained with RoCE with Arista 7800 + Minipack2 OCP TORs</li>
   <li>24K GPUs in 3-layer Clos:
      <ol type="i"> 
      <li>Lowest layer: Each rack has x2 servers (16 GPUs) connected with Minipack2 </li>
      <li>Middle layer: Each "pod" has 192 racks are connected by cluster switches to form 3072 GPUs at full bandwidth (no OS)</li>
      <li>Top layer: 8x pods connected via aggregation switches to form 24K cluster. OS is at 1:7 </li>
      </ol>
    </li>
    <li>Designed model parallelism methods and scheduler to be topology aware and minimize cross-pod traffic</li>
    <li>Smaller models were trained with QM2 Infiniband based cluster</li>
    <li>Network software uses Equal-Cost Multi-Path (ECMP) load balancing and congestion control with deep-buffer switches in the spine</li>
    <li>Both clusters have 400Gbps interconnects</li>
    </ol>
1. Orchestration
   <ol type="a">
   <li>Using MAST training scheduler: https://www.usenix.org/system/files/osdi24-choudhury.pdf</li>
   </ol>
1. Model scaling
   <ol type="a">
   <li>Uses 4D parallelism -- combination of TP, PP, context-parallelism (divides input context into segments) and FSDP (shards model, optimizer and gradients along with data)</li>
   <li>For network-awareness, place the dimensions in the order of [TP, CP, PP, DP]. DP is the outermost as it can tolerate longer network latency. </li>
   <li>Achieve BF16 MFU of 38% (16K) - 43% (8K); 380-430 TFLOPs/GPU</li>
      <ol type="i">
      <li>Gradient accumulation happens in FP32</li>
      </ol>
   </ol>
1. Collectives
   <ol type="a">
   <li>Fork of NCCL called NCCLX</li>
      <ol type="i">
      <li>Tunes chunking and data copies to fit network latencies</li>
      <li>Priority for smaller control messages</li>
      </ol>
    </ol>
1. Reliability
   <ol type="a">
   <li>Single GPU failure results in restarts of the entire job</li>
   <li>Achieved 90% training time -- but at least one interruption daily during the 54-day period of pre-training llama3 405B</li>
   <li>Total of 466 job interruptions -- 47 were planned (for automated maintenance including firmware upgrades or dataset/config) and 419 were unplanned</li>
   <li>Failure analysis<li>
      <ol type="i">
      <li>HBM3 memory accounted for 72/419 or 17.2% failure rate</li>
      <li>Faulty GPU was 148/419 or 30.1% failure rate</li>
      <li>There were separate rows for SRAM, GSP, SDC, Thermal interface -- so its unclear what the "Faulty GPU" here represents(perhaps vreg, inforom, or some other HGX board issue)</li>
      </ol>
   <li>Use PyTorch NCCL flight recorder to capture metadata and stack traces in a ring-buffer </li>
   <li>Noted a diurnal 1-2% throughput variation based on time-of-day. Due to higher mid-day temperatures impacting GPU dynamic voltage & frequency scaling</li>
   <li>Power fluctuations (thousands of GPUs +/- power consumption due to checkpoint waits or collectives to finish, startup/shutdown of entire training job) across the datacenter on the order of tens of megawatts </li>


