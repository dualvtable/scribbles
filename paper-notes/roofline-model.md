## Introduction

This document is a set of maintained notes about the roofline-model  
for performance. 

> "Roofline: An Insightful Visual Performance Model for  
> Floating-Point Programs and Multicore Architectures" 


Here are the resources:
1. The main technical [report](https://www2.eecs.berkeley.edu/Pubs/TechRpts/2008/EECS-2008-134.pdf).
1. [Slides](https://crd.lbl.gov/assets/pubs_presos/parlab08-roofline-talk.pdf) from a tutorial talk.

### Main thesis

The roofline model is an easy-to-understand visual  
representation of a "performance model" for parallel  
software and hardware esp. when dealing with  
floating-point computations.

The performance model is esp. important to understand  
how processor computational performance of a given program  
(i.e. whether its *compute-bound*) relates to memory traffic  
(i.e. whether its *memory-bound*).

To that end, the roofline model defines *operational intensity*,  
which is used to measure operations per byte of DRAM traffic.  
This is then plotted against the floating-point performance of the  
hardware on a 2-D plot:

1. The Y-axis represents the floating-point performance (attainable  
   GFLOPs/s)
1. The X-axis represents the operational intensity (FLOPs/byte)


Then, the roofline limits are created once per multicore computer --  
the horizontal line is the peak attainable GLOPs of the hardware  
and the 45-degree line represents the peak memory bandwidth.

The *ridge point*, which is the intersection of the horizontal and  
the diagnoal roofs gives an insight into the overall performance of a  
system: 

* If the ridge point is far to the right, then programs/kernels  
  need high operational intensity to achieve performance gains.
* If the ridge point is far to the left, then any program/kernel  
  can potentially hit the maximum performance. 

