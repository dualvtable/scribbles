# Multi-Process Service

MPS allows multiple CUDA processes to be processed concurrently on the GPU, 
thereby improving utilization when the GPU compute capacity may not be saturated 
by a single process. More information can be found in the official MPS 
[documentation](https://docs.nvidia.com/deploy/mps/index.html).

## MPS with Docker

Provisioning steps for running containers with MPS:

1. Setup exclusive mode on the GPU:
    ```bash
    sudo nvidia-smi -c EXCLUSIVE_PROCESS
    ```
1. Setup the socket in a temp directory:
    ```bash
    export CUDA_MPS_PIPE_DIRECTORY=/tmp/nvidia-mps \
    && sudo CUDA_MPS_PIPE_DIRECTORY=/tmp/nvidia-mps nvidia-cuda-mps-control -d
    ```
1. Run the desired CUDA application:
    ```bash
    sudo docker run --gpus all \
      -v $CUDA_MPS_PIPE_DIRECTORY:$CUDA_MPS_PIPE_DIRECTORY \
      -e $CUDA_MPS_PIPE_DIRECTORY \
      --ipc=host \
      nvidia/samples:dcgmproftester-2.0.10-cuda11.0-ubuntu18.04 \
      --no-dcgm-validation -t 1004 -d 30
    ```
1. Verify that the MPS client is running on the system using `nvidia-smi`. You should be able to see `M+C` as the process type:
    ```console
    +-----------------------------------------------------------------------------+
    | Processes:                                                                  |
    |  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
    |        ID   ID                                                   Usage      |
    |=============================================================================|
    |    0   N/A  N/A     36686      C   nvidia-cuda-mps-server             25MiB |
    |    0   N/A  N/A     37092    M+C   /usr/bin/dcgmproftester11         315MiB |
    +-----------------------------------------------------------------------------+
    ```

### Resource Limits

#### Memory Limits 

Since CUDA 11.5, MPS has supported the use of memory limits (global, per server and per client). More information on the memory limits can be found in the documentation on [`CUDA_MPS_PINNED_DEVICE_MEM_LIMIT](https://docs.nvidia.com/deploy/mps/index.html#topic_5_2_7). 

In the following examples, let's test the usage of per-client memory limits. The `CUDA_MPS_PINNED_DEVICE_MEM_LIMIT` variable controls the maximum amount of memory that is available for allocation by the MPS client. The `CUDA_MPS_PINNED_DEVICE_MEM_LIMIT` variable follows a particular format where the value string can contain comma separated device ordinals and/or device UUIDs with per device memory limit separated by an "equals(=)". Memory limits can be specified with "M" for Megabytes and "G" for Gigabytes. If no or any other literal is specified, the number will be read in as Megabytes.

For example:

```bash
$export CUDA_MPS_PINNED_DEVICE_MEM_LIMIT="0=1G,1=2G,GPU-7ce23cd8-5c91-34a1-9e1b-28bd1419ce90=1024M"
```

##### Examples

The `example/` directory contains a contrived CUDA example that can be used to test the amount of free memory available for allocation by the MPS client. The free memory can be reported using the `cudaMemGetInfo` API. 

We can set a limit of 300M that can be allocated by the MPS client:

```bash
$ sudo docker run --gpus all \
   -v $CUDA_MPS_PIPE_DIRECTORY:$CUDA_MPS_PIPE_DIRECTORY \
   -e $CUDA_MPS_PIPE_DIRECTORY \
   -e CUDA_MPS_PINNED_DEVICE_MEM_LIMIT="0=300M" \
   --ipc=host \
   dualvtable/cuda-mem-get-info
```

From the console output, you can see:

```console
For device 0 ;  Free memory: 204 M, Total memory: 14960 M
```

In the next example, we launch a GEMM workload, but limit the amount of allocatable memory to 300MB (to observe an out-of-memory error). 

```bash
$ sudo docker run --gpus all \
  -v $CUDA_MPS_PIPE_DIRECTORY:$CUDA_MPS_PIPE_DIRECTORY \
  -e $CUDA_MPS_PIPE_DIRECTORY \
  -e CUDA_MPS_PINNED_DEVICE_MEM_LIMIT="0=300M" \
  --ipc=host \
  us-west2-docker.pkg.dev/egx-anthos-gpu-operator-val/gst/v2:ubuntu-18.04
```

From the console output, you can see the out-of-memory error:

```console
/nvidia/gst capturing GPU information...
WATCHDOG starting, TIMEOUT: 600 seconds
Detected 1 CUDA Capable device(s)
Device 0: "Tesla T4"
Initilizing T4 based test suite
GPU Memory: 14, memgb: 16


Device 0: "Tesla T4", PCIe: 0

***** STARTING TEST 0: FP16 On Device 0 Tesla T4
#### math_type 0
#### args: matrixSizeA 2813718656 matrixSizeB 3412772992 matrixSizeC 1231479872
std::exception: out of memory
testing cublasLt fail
```

## MPS with Kubernetes

In this specific deployment, we're going to start the MPS control daemon on the host. 

1. Setup exclusive mode on the GPU:
   ```bash
   sudo nvidia-smi -c EXCLUSIVE_PROCESS
   ```    
1. Next start the control daemon on the host system:
    ```bash
    sudo CUDA_MPS_PIPE_DIRECTORY=/tmp/nvidia-mps nvidia-cuda-mps-control -d
    ```
1. Create a deployment yaml that shares the host IPC namespace and volume mounts from the host as shown below. 
   Note that in this case, the deployment bypasses the NVIDIA device plugin and requests for devices directly using 
   the `NVIDIA_VISIBLE_DEVICES` environment variable.

   ```bash
   cat << EOF | kubectl create -f -
   apiVersion: batch/v1
   kind: Job
   metadata:
     name: cuda-sample1
   spec:
     template:
       spec:
         hostPID: true
         hostIPC: true
         containers:
           - name: cuda-sample-container1
             image: nvidia/samples:nbody
             command: ["/tmp/nbody"]
             args: ["-benchmark", "-i=1000"]
             env:
             - name: NVIDIA_VISIBLE_DEVICES
               value: "0"
             - name: CUDA_MPS_PIPE_DIRECTORY
               value: "/tmp/nvidia-mps"
             volumeMounts:
             - mountPath: /tmp/nvidia-mps
               name: mps-pipe
         volumes:
         - name: mps-pipe
           hostPath:
              path: /tmp/nvidia-mps
              type: Directory
         restartPolicy: "Never"
     backoffLimit: 1
   ---

   apiVersion: batch/v1
   kind: Job
   metadata:
     name: cuda-sample2
   spec:
     template:
       metadata:
       spec:
         hostPID: true
         hostIPC: true
         containers:
           - name: cuda-sample-container2
             image: nvidia/samples:nbody
             command: ["/tmp/nbody"]
             args: ["-benchmark", "-i=1000"]
             env:
             - name: NVIDIA_VISIBLE_DEVICES
               value: "0"
             - name: CUDA_MPS_PIPE_DIRECTORY
               value: "/tmp/nvidia-mps"
             volumeMounts:
             - mountPath: /tmp/nvidia-mps
               name: mps-pipe
         volumes:
         - name: mps-pipe
           hostPath:
              path: /tmp/nvidia-mps
              type: Directory
         restartPolicy: "Never"
     backoffLimit: 1
   EOF
   ```
1. We can verify using `nvidia-smi` that the `nvidia-cuda-mps-server` has been started and the two applications run as MPS clients. 
   ```console
    +-----------------------------------------------------------------------------+
    | Processes:                                                                  |
    |  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
    |        ID   ID                                                   Usage      |
    |=============================================================================|
    |    0   N/A  N/A    333723      C   nvidia-cuda-mps-server             25MiB |
    |    0   N/A  N/A    411839    M+C   /tmp/nbody                        107MiB |
    |    0   N/A  N/A    411840    M+C   /tmp/nbody                        107MiB |
    +-----------------------------------------------------------------------------+
   
   ```

### MicroK8s 

This section includes information on running with MPS under [MicroK8s](https://microk8s.io/docs/getting-started).

### Pre-requisites

Before installing MicroK8s, ensure that the NVIDIA drivers are installed on the system and `nvidia-smi` is functional (i.e. kernel modules can be loaded, GPUs enumerated on `/dev` and CUDA can be initialized). 

Follow the documentation to install MicroK8s. To get access to GPUs and use MPS, two add-ons are required:
1. [GPU add-on](https://microk8s.io/docs/addon-gpu) : This add-on installs the gpu-operator, which automatically installs a number of GPU components such as the NVIDIA device plugin and the NVIDIA container toolkit (aka `nvidia-docker2`). 
1. [host-access](https://microk8s.io/docs/addon-host-access)

### Running MPS with MicroK8s

Once the MicroK8s add-ons are installed, the cluster should look something like shown below:
```bash
$ microk8s kubectl get pods -A
NAMESPACE                NAME                                                          READY   STATUS      RESTARTS   AGE
kube-system              calico-kube-controllers-784b988f59-s2qpp                      1/1     Running     0          5h7m
kube-system              calico-node-hddr9                                             1/1     Running     0          5h7m
kube-system              coredns-7f9c69c78c-l475t                                      1/1     Running     0          5h3m
default                  gpu-operator-node-feature-discovery-worker-9ds9t              1/1     Running     0          5h1m
default                  gpu-operator-node-feature-discovery-master-867c4f7bfb-pnc24   1/1     Running     0          5h1m
default                  gpu-operator-7db468cfdf-n897j                                 1/1     Running     0          5h1m
gpu-operator-resources   nvidia-container-toolkit-daemonset-8ntn4                      1/1     Running     0          5h1m
gpu-operator-resources   nvidia-cuda-validator-57t5d                                   0/1     Completed   0          5h1m
gpu-operator-resources   nvidia-device-plugin-daemonset-2rz8z                          1/1     Running     0          5h1m
gpu-operator-resources   nvidia-dcgm-exporter-f2j2h                                    1/1     Running     0          5h1m
gpu-operator-resources   nvidia-device-plugin-validator-t9rg2                          0/1     Completed   0          5h
gpu-operator-resources   nvidia-operator-validator-z4nml                               1/1     Running     0          5h1m
gpu-operator-resources   gpu-feature-discovery-rddc9                                   1/1     Running     0          5h1m
```

Next, setup the exclusive mode and the MPS control daemon as described in the earlier part of the documentation. 

Finally, deploy two pods which run under the MPS server (these pods run FP16 GEMMs on the GPU):

```bash
cat << EOF | microk8s kubectl create -f -
apiVersion: v1
kind: Pod
metadata:
  name: dcgmproftester-1
spec:
  restartPolicy: "Never"
  hostPID: true
  hostIPC: true   
  containers:
  - name: dcgmproftester11
    image: nvidia/samples:dcgmproftester-2.0.10-cuda11.0-ubuntu18.04
    args: ["--no-dcgm-validation", "-t 1004", "-d 30"]  
    securityContext:
      capabilities:
        add: ["SYS_ADMIN"]    
    env:
    - name: NVIDIA_VISIBLE_DEVICES
      value: "0"
    - name: NVIDIA_DISABLE_REQUIRE
      value: "1"
    - name: CUDA_MPS_PIPE_DIRECTORY
      value: "/tmp/nvidia-mps"
    volumeMounts:
    - mountPath: /tmp/nvidia-mps
      name: mps-pipe
  volumes:
  - name: mps-pipe
    hostPath:
       path: /tmp/nvidia-mps
       type: Directory
---

apiVersion: v1
kind: Pod
metadata:
  name: dcgmproftester-2
spec:
  restartPolicy: "Never"
  hostPID: true
  hostIPC: true   
  containers:
  - name: dcgmproftester11
    image: nvidia/samples:dcgmproftester-2.0.10-cuda11.0-ubuntu18.04
    args: ["--no-dcgm-validation", "-t 1004", "-d 30"]  
    securityContext:
      capabilities:
        add: ["SYS_ADMIN"]    
    env:
    - name: NVIDIA_VISIBLE_DEVICES
      value: "0"
    - name: NVIDIA_DISABLE_REQUIRE
      value: "1"
    - name: CUDA_MPS_PIPE_DIRECTORY
      value: "/tmp/nvidia-mps"
    volumeMounts:
    - mountPath: /tmp/nvidia-mps
      name: mps-pipe
  volumes:
  - name: mps-pipe
    hostPath:
       path: /tmp/nvidia-mps
       type: Directory
EOF
```

When `nvidia-smi` is run in another console, the two containers can be seen running as MPS clients:
```bash
+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
|    0   N/A  N/A    266560      C   nvidia-cuda-mps-server             23MiB |
|    0   N/A  N/A    290556    M+C   /usr/bin/dcgmproftester11         313MiB |
|    0   N/A  N/A    290557    M+C   /usr/bin/dcgmproftester11         313MiB |
+-----------------------------------------------------------------------------+
```
