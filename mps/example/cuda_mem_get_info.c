/****************************************************************************
 * Copyright (c) 2022 NVIDIA Corporation.  All rights reserved.
 * 
 ****************************************************************************/

#include <cuda.h>
#include <cuda_runtime.h>

#include <dlfcn.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define CHECK(x, err, errcode) do { \
  int retval = (x); \
  if (retval != 1) { \
    fprintf(stderr, "Error: %s returned %d at %s:%d\n", #x, retval, __FILE__, __LINE__); \
    return(errcode); \
  } \
} while (0)


int main(int argc, const char **argv)
{
        int deviceCount = 0;
        cudaError_t error_id = cudaGetDeviceCount(&deviceCount);
        if (error_id != cudaSuccess) 
        {
                fprintf(stderr, "cudaGetDeviceCount returned: %d\n -> %s\n", 
                        (int)(error_id), cudaGetErrorString(error_id));
                return((int)(error_id));
        }

        if (deviceCount == 0)
        {
                printf("No GPU devices found\n");
                return 1;
        }

        for (int i = 0; i < deviceCount; i++)
        {
                cudaSetDevice(i);
                size_t freeMem = 0; 
                size_t totalMem = 0;
                error_id = cudaMemGetInfo(&freeMem, &totalMem);
                if (error_id != cudaSuccess) 
                {
                        fprintf(stderr, "cudaMemGetInfo returned: %d\n -> %s\n", 
                        	(int)(error_id), cudaGetErrorString(error_id));
                	return((int)(error_id));
                }
                printf("For device %d ;  Free memory: %d M, Total memory: %d M\n", i, freeMem/(1024*1024), totalMem/(1024*1024));
        }
        return 0; 
}
