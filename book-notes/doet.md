## Design of everyday things
Book on design by Dan Norman
Summary [notes](https://medium.com/@DEJIDOPE/summary-of-design-of-everyday-things-by-don-norman-f06f023fdc95)

### Chapter 1

Fundamental principles of design: 

- provide a good conceptual model
- make things visible 

Properites of a good design:

1. Visibility: Parts must be visible and they should provide the  
   correct message. But be wary of visiblity -- lack of visibility  
   makes devices difficult to operate; while excess visibility may  
   make devices too feature-laden and intimidating. Also applies to  
   the concept of *feedback* -- send information back to the user  
   about what action has been done and what result has been accomplished.

1. Affordances: Refers to the fundamental properties that determine  
   how something can be used. 

1. Constraints: Physical constraints that limit what can be done or  
   semantic/cultural constraints that provide clues for further decisions.

1. Mappings: Natural relationships between the controls and things  
   being controlled. A powerful way to do this is to take advantage  
   of physical analogies (move control up -> move object up; arrange  
   light controls in the same pattern as lights)

### Chapter 3

How do people interact with the world? 

* Declarative knowledge: Easy to write down and teach. 
* Procedural knowledge: Subconscious, and learned over time through  
  skills e.g. perform music; thought by demonstration and learned  
  through practice. 

This is particularly applicable when users encounter a new object  
-- how does the user know how to interact with the object? 

* Either the knowledge is in the head => user has dealt with something  
similar in the past and can transfer the knowledge to the new experience. 
* Or the other approach is the use the knowledge in the world => if the  
design of the object has presented some information that can be interpreted.

### Chapter 5

#### Designing for errors 

1. Understand the causes of error and design to minimize those causes
2. Make it possible to reverse actions—to "undo" them—or make it harder
   to do what cannot be reversed.
1. Make it easier to discover the errors that do occur, and make them  
   easier to correct.
1. Change the attitude toward errors. Think of an object's user as  
   attempting to do a task, getting there by imperfect approximations.  
   Don't think of the user as making errors; think of actions as the  
   approximations of what is desired. 

#### Forcing functions

Form of physical constraint which prevent the next step from happening  
if the current step fails. Some other terms -- 

* interlocks -- force operations in sequence
* lockin -- prevnt operation from ending prematurely
* lockout -- prevents an event from occurring 

In the end, desinging for errors follows some good design principles:

* Put the required "knowledge in the world" instead of relying on users'  
  experience ("knowledge in the head").
* Use forcing functions and natural mappings.
* Make things visible -- both for operation of something and provide feedback.

### Chapter 6

Summary of good design principles:

* Make it easy to determine what actions are possible (i.e. use constraints,  
  including natural properties of people/world).
* Make things visible, including the conceptual model of the system, alternative  
  actions and results of the actions
* Make it easy to evaluate the current state of the system
* Follow natural mappings between intentions and required actions




