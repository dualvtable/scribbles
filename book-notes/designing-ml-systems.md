## Introduction

Personal notes on the book: <br>
Designing Machine Learning Systems <br>
by Chip Huyen 2022

### Resources 
For further reading:
1. [Architecture of ML Systems SS2019 course](https://mboehm7.github.io/teaching/ss19_amls/)

### Chapter 1 -- Overview

- Introduction to ML systems
  - *ML models* (clustering, logistic regression, NN architectures  
    such as feed-forward, recurrent, convolutional, transformer)
  - *ML techniques* (supervised vs. unsupervised learning, gradient  
    descent, hyperparameter tuning)
  - *Metrics* (accuracy, precision, MSE)
  - *Statistical concepts* (variance, probability, normal distribution)
  - *Common tasks* (language, object classification, sequence prediction)
- Throughput vs. latency (commonly check for  
  p90, p95 and p99 latency in systems to detect bad latency outliers)

### Chapter 2 -- ML Systems Design

- The four requirements of ML systems
  - reliability
    - how to check if a model can predict "correctly"  
      (i.e. what ground truth labels are available?).
  - scalability
    - ML system can grow in model complexity (parameters  
      / memory requirements), traffic volume, model count.
  - maintainability
    - code, artifacts, data should be versioned for easy  
      maintenance. 
  - adaptability 
    - systems should have capacity for discovering aspects  
      for performance improvements and updates without  
      service disruption.
  - iterative process for developing ML systems in production
    ![](assets/imgs/ml-dev-process.png)

- Stack of ML Systems, which is helpful in thinking about the  
  optimization opportunities at various levels:
  ![](assets/imgs/ml-stack-systems.png)

### Chapter 3 -- Data Engineering

- User data (is generally malformed) vs. system generated (e.g. logs)
- Data formats
  - JSON / CSV (row-major); Parquet (column-major) but binary
    - row-major is better for lots of writes; column-major better for  
      lots of column-based reads
- Data models
  - Relational vs. nonrelational databases
    - document model (JSON)
    - graph model (where data is represented as nodes and relationships  
      as edges)
  - Structured (less flexible) vs. unstructured data
- Data storage engines
  - Transactional processing
  - ETL
- Dataflow modes
  - data passing through databases
  - data passing through services (REST and gRPC)
  - data passing through real-time transport
    - event-driven paradigm
      - pubsub (apache kafka, amazon kinesis)
      - message queue (rabbitmq)
  - batch vs. stream processing


### Chapter 4 -- Training Data

- Sampling data to create splits (for training, validation)
- Labeling; used for supervised learning
  - hand labeled (need sme; has privacy issues; is slow)
  - natural labels (e.g. recommender systems)

### Chapter 5 -- Feature Engineering

TBD

### Chapter 6 --- Model Development 

- Evaluating ML models
  - Classic ML models (e.g. logistic regression, gradient-boosted  
    trees etc.) still are used in production
  - To solve ML tasks, focus on known ML models -- e.g. text classification  
    can be done by BERT, recurrent neural networks etc. 
  - Other factors in considering which model to choose:
    - model's performance (e.g. accuracy)
    - how much data/compute it needs to be trained
    - inference latency and interpretability
- Ensembles
  - Use ensembles of models to get a performance/accuracy boost (final prediction  
    is a majority vote among the models); however they are difficult  
    to deploy and hard to maintain
- Experiment tracking and versioning
  - Use MLflow, Weigths&Biases to track experiments during model development
  - Challenges with code versioning, data versioning and debugging ML models  
    (which requires coordination between features, parameter tuning, infra etc.)
- Distributed training
  - Data parallelism
    - split data across machines; train model on all of them and then  
      accumulate gradients 
    - this leads to two paradigms -- synchronous SGD (wait for all machines  
      to finish) and asynchronous SGD (use gradient from each machine  
      separately)
  - Model parallelism 
    - model layers are split across different machines
    - pipeline parallelism is an optimization to make model parallelism more 
      efficient 
  
### Chapter 7 --- Model Deployment

- Easy to wrap a model's `predict` function in a POST endpoint using Flask  
  or FastAPI and deploy using a container
- But hard to scale to millions of users with latency and 99% uptime guarantees
- Myths
  - Multiple models are deployed in production
  - Models suffer from data distribution drifts; thus need constant updates
  - Think about scalability in production
- Three main models of prediction
  - batch -> use only batch features
  - online -> use only batch features
  - online -> use either batch or streaming features
- Model compression to make inference go faster
  - Low rank factorization -> replace high-dimensional tensors with lower-dimensional
  - Knowledge distillation -> small model (student) trained to mimic a larger model (teacher)
    Method is sensitive to applications/model architectures and isn't used widely in  
    production.
  - Pruning -> reduce number of non-zero parameters (redundant and non-critical for classification)  
    Find parameters least useful and set them to 0. IOW, makes models more sparse 
  - Quantization -> reduce precision for size & computation 
    - Downsides are that lower precision reduces range, requiring rounding
    - Rounding introduces errors 
  - Model optimization
    - vectorization
    - parallelization
    - loop tiling
    - fusion
    
### Chapter 8 --- Distribution Shifts and Monitoring

- Important to analyze data distribution shifts of models in production over time. 
- ML specific failure examples
  - data collection/processing problems
  - poor hyperparameters
  - training changes not reflected in inference pipelines
- Some real ML model failures in production
  - Production data differing from training data
  - Edge cases
  - Degenerate feedback loops (aka bias)
    - Randomization
    - Positional features
- Data distribution shifts
  - Data that the model works with changes over time which causes the model's 
    prediction to become less accurate as time passes.
  - Detecting shifts
    - check metrics such as accuracy, F1 score, recall etc. 
    - use statistical methods, time-scale windows to detect distribution shifts
  - Addressing shifts
    - first way -- train model on a very large dataset and hope that the distribution  
      learned is comprehensive to cover any data encountered in production
    - second way -- adapt a trained model to target distribution without requiring  
      new labels
    - third way -- re-train the model using labeled data from the target distribution  
      (or fine-tuning)
- Monitoring and observability
  - ML systems are software systems, so we use typical metrics:
    - throughput / latency / number of requests / precent returned with a 2xx code
    - CPU/GPU utilization / memory utilization
    - uptime
  - ML model specific metrics
    - accuracy, predictions, failures and raw inputs
    - monitoring features
    - monitoring raw inputs
  - How to monitor
    - logs
    - dashboards
    - alerts

### Chapter 9 --- Continual Learning and Test in Production

- Continual learning
  - create replica of existing model, update replica on new data  
    and then replace existing model *iff* the replica proves to be  
    better
  - stateless or stateful retraining
    - stateless means train from scratch each time
    - stateful is fine-tuning -- has many advantages including faster  
      convergence, and saving compute cost
    - *cold start problem* -- when a recommender model knows nothing about  
      the user and needs to start making predictions quickly before the user  
      loses interest
  - challenges
    - access to fresh data (answer may be extracting labels while streaming data?)
    - evaluation challenge (thourougly test update before deployment)
    - algorithm challenge (some models are algorithmically harder to update  
      e.g. matrix-based)
  - four stages of continual learning
    - manual stateless retraining
    - automated retraining
    - automated stateful retraining
    - continual learning (requires triggers for time-based, volume-based, drift-based, 
      or performance-based)
  - testing models 
    - do a shadow deployment of the candidate model (infra is doubled)
    - A/B testing two variants (% of traffic is routed between variants)
    - canary release
    - interleaving results from variants to users
    - use bandit algorithms for stateful routing between variants


### Chapter 10 --- Infra and Tooling for MLOps

#### Different layers of the infra for ML

  ![](assets/imgs/ml-infra-stack.png)

- Compute unit is characterized by two metrics: memory and FLOPS  
  (see roofline model for understanding performance of a system)

- Argues that data storage and compute layers are commoditized  
  and it makes sense for companies to optimize TCO for ML workloads  
  that are bursty to use cloud providers for compute. CSPs offer  
  elasticity (and offloads capital costs of building on-prem compute).

- Counter to that argument is that when specific companies such as  
  Dropbox grow in scale, there is a "cloud repartiation", where these  
  scale companies move to own datacenters. This can be expensive. 

- Dev environment includes versioning for code (git), data (DVC), track  
  experiments (Weights & Biases), models (MLflow) for deployment.  
  Standardizing dev environments (including software versions, virtual envs,  
  Python package versions etc.) improves productivity and reproducibility.

- Use containers for portability and going from dev -> production!

#### Resource Management

- Two key characteristics of ML influence resource management: repetitiveness  
  and dependencies 

  - Some notes about orchestrators (which deal with lower-level machines,  
    resources, grouping, replication, self-healing etc.), and schedulers  
    (which use DAGs forworkflow dependencies, queues, priorities, quotas)

- Data science workflow management tools
  - Airflow 
    - Python instead of YAML
    - Packages entire workflow into a container -> if different steps have  
      different requirements, managing different containers is challenging
    - Not parameterized 
    - DAGs are static
  - Prefect
    - Supports parameterized workflows
    - Containers are not first class
  - Argo
    - Messy YAML files
    - Only K8s 
  - Kubeflow and Metaflow
    - Support both dev & prod by abstracting infra boilerplate
    - Kubeflow still requires Dockerfiles and YAML
    - Metaflow -> simply decorate code (libraries, compute, memory) and Metaflow  
      handles containerization
  
  #### Model Deployment and Store

  - Think about various SaaS for ML deployment from CSPs and startups 
  - Need to handle both online and batch (online is easy since you get single  
    request/response; vs. batch means you need to handle batch jobs and results store)
  - Properties of models that are deployed into production
    - Model definition (shape, loss function, layers, hidden layers and parameters  
      in each layer)
    - Model parameters (values of the parameters used in the model)
    - Featurize and predict functions (extract features from data that needs to be input)
    - Dependencies (package dependencies used to run the model)
    - Data (used to train the model)
    - Model generation code 
    - Experiment artifacts (e.g. loss curve)
    - Tags (owner of the model, business objective of the model)


